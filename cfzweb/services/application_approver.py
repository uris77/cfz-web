from cfzpersistence.repository.application import ApplicationRepository
from cfzweb.model import DBSession
from cfzcore.approvers import ApplicationApprover


def approve(application_id,
            repository=ApplicationRepository(DBSession),
            approver=ApplicationApprover()):
    """Approve an Application Submission"""
    application = repository.get(int(application_id))
    approver.approve_application(application)
    repository.persist(application)
    return application


def deny(application_id, reason_for_denial,
         repository=ApplicationRepository(DBSession),
         approver=ApplicationApprover()):
    """Dey an Application"""
    application = repository.get(int(application_id))
    approver.deny_application(application, reason_for_denial)
    repository.persist(application)
    return application.company.name
