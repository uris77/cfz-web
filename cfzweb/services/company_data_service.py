from cfzpersistence.repository.company import CompanyRepository
from cfzcore.interactors.company import CompanyAddress, CompanyContactNumbers
from cfzcore.interactors.company import update_manager as update_company_manager
from cfzweb.model import DBSession


class CompanyService(object):
    def __init__(self, company_repository=CompanyRepository(DBSession)):
        self.company_repository = company_repository

    def get_with_id(self, company_id):
        company = self.company_repository.get(company_id)
        return company

    def convert_shareholders_to_list(self, shareholders):
        _shareholders = []
        for shareholder in shareholders:
            _shareholder = dict(id=shareholder.id,
                                first_name=shareholder.person.first_name,
                                last_name=shareholder.person.last_name,
                                shares=shareholder.shares,
                                background_check=shareholder.background_check)
            _shareholders.append(_shareholder)
        return _shareholders

    def update_address(self, company_id, **kwargs):
        company_address = CompanyAddress(company_id, self.company_repository)
        company_address.update(**kwargs)

    def update_contact_numbers(self, company_id, **kwargs):
        company_numbers = CompanyContactNumbers(company_id, self.company_repository)
        company_numbers.update(**kwargs)

    def update_manager(self, company_id, **kwargs):
        update_company_manager(company_id, self.company_repository, **kwargs)
