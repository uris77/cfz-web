import unittest
from mock import Mock
from collections import namedtuple

from cfzweb.services.company_data_service import CompanyService


class CompanyRecordRetrieverTests(unittest.TestCase):
    def setUp(self):
        self.repository = Mock()
        self.company = Mock()
        CompanyRow = namedtuple("CompanyRow", "Company")
        retrieved_company = CompanyRow(Company=self.company)
        self.repository.get.return_value = retrieved_company
        self.company_service = CompanyService(self.repository)

    def test_get_company_record_from_repository(self):
        company_id = 1
        self.company_service.get_with_id(company_id)
        self.repository.get.assert_called_once_with(company_id)

    def test_update_address_persists_company(self):
        company_id = 1
        address = dict(state="Corozal", country="Belize")
        self.company_service.update_address(company_id, **address)
        self.repository.get.assert_called_once_with(company_id)
        self.repository.persist.assert_called_once_with(self.company)

    def test_update_contact_numbers(self):
        company_id = 1
        contact_numbers = dict(telephone_number="1234",
                               fax_number="5678",
                               cable_number="9012")
        self.company_service.update_contact_numbers(company_id, **contact_numbers)
        self.repository.get.assert_called_once_with(company_id)
        self.repository.persist.assert_called_once_with(self.company)
