import unittest
from mock import Mock
from cfzweb.services import application_approver as approver_service


class ApplicationApproverServiceTests(unittest.TestCase):
    def test_approve_an_application(self):
        repository = Mock()
        application = Mock()
        approver = Mock()
        repository.get.return_value = application
        application_id = 1
        approver_service.approve(application_id, repository, approver)
        repository.get.assert_called_once_with(application_id)
        repository.persist.assert_called_with(application)
        approver.approve_application.assert_called_once_with(application)

    def test_deny_an_appliation(self):
        repository = Mock()
        application = Mock()
        approver = Mock()
        repository.get.return_value = application
        reason_for_denial = "Failed Background Check"
        application_id = 1
        approver_service.deny(application_id, reason_for_denial, repository, approver)
        repository.get.assert_called_once_with(application_id)
        repository.persist.assert_called_once_with(application)
        approver.deny_application.assert_called_once_with(application, reason_for_denial)
