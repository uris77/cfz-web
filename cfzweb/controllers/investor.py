from tg import expose, request

from cfzweb.lib.base import BaseController
from cfzweb.model import DBSession
from cfzcore import InvestorBackgroundChecker

from cfzpersistence.repository.investor import InvestorRepository

import simplejson


class InvestorController(BaseController):

    def __init__(self):
        self.repository = InvestorRepository(DBSession)

    @expose('json')
    def passed(self):
        submitted_data = simplejson.loads(request.body)
        background_checker = InvestorBackgroundChecker(self.repository)
        investor = background_checker.save_background_check(submitted_data["id"], 'P')
        return dict(passed=investor)

    @expose('json')
    def failed(self):
        submitted_data = simplejson.loads(request.body)
        background_checker = InvestorBackgroundChecker(self.repository)
        investor = background_checker.save_background_check(submitted_data["id"], 'F')
        return dict(failed=investor)
