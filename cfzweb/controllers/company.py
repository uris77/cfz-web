from tg import expose, request
from cfzweb.lib.base import BaseController
from cfzweb.presenters.company import (CompanySummaryPresenter,
                                       CompanyDetailsPresenter,
                                       AnnualReportPresenter)
from cfzweb.presenters import ShareholderPresenter
from cfzweb.model import DBSession
from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository import Repository
from cfzcore.interactors.company import (CompanyShareholder,
                                         add_annual_report_for_company)

from cfzweb.services.company_data_service import CompanyService

import jsonpickle
import simplejson
from datetime import datetime


class CompanyController(BaseController):

    @expose()
    def _lookup(self, id, sub_path, *args):
        if sub_path == 'address':
            address = AddressController(id)
            return address, args
        if sub_path == 'contact_numbers':
            contact_numbers = ContactNumbersController(id)
            return contact_numbers, args
        if sub_path == 'manager':
            manager = ManagerController(id)
            return manager, args
        if sub_path == 'shareholder':
            shareholder = ShareholderController(id)
            return shareholder, args
        if sub_path == 'date_letter_sent':
            date_letter_sent = DateLetterSentController(id)
            return date_letter_sent, args
        if sub_path == 'annual_report':
            annual_report = AnnualReportController(id)
            return annual_report, args

    @expose("cfzweb.templates.company.list")
    def index(self):
        return dict()

    @expose()
    def list(self):
        repository = CompanyRepository(DBSession)
        companies = repository.all_authorized_companies()
        companies_list = list(CompanySummaryPresenter(company).__dict__ for company in companies)
        return jsonpickle.encode(companies_list)

    @expose("cfzweb.templates.company.selectbox")
    def selectbox(self):
        repository = CompanyRepository(DBSession)
        companies = repository.all_authorized_companies()
        return dict(companies=companies)

    @expose()
    def search_with_name(self, company_name):
        repository = CompanyRepository()
        companies = repository.search_for(company_name)
        companies_list = list(CompanySummaryPresenter(company).__dict__ for company in companies)
        return jsonpickle.encode(companies_list)

    @expose("cfzweb.templates.company.show")
    def show(self, id):
        return dict()

    @expose('json')
    def fetch(self, id):
        company_service = CompanyService()
        results = company_service.get_with_id(id)
        company = CompanyDetailsPresenter(results)
        return jsonpickle.encode(company)


class AddressController(BaseController):
    def __init__(self, id):
        self.repository = Repository(DBSession)
        self.company_service = CompanyService()
        self.company_id = id

    @expose("cfzweb.templates.company.address_update_success")
    def save(self):
        address = simplejson.loads(request.body)
        self.company_service.update_address(self.company_id, **address)
        return dict()


class ContactNumbersController(BaseController):
    def __init__(self, id):
        self.repository = Repository(DBSession)
        self.company_service = CompanyService()
        self.company_id = id

    @expose("cfzweb.templates.company.contact_numbers_success")
    def save(self):
        contact_numbers = simplejson.loads(request.body)
        self.company_service.update_contact_numbers(self.company_id, **contact_numbers)
        return dict()


class ManagerController(BaseController):
    def __init__(self, id):
        self.company_id = id
        self.repository = CompanyRepository(DBSession)
        self.company_service = CompanyService()

    @expose("cfzweb.templates.company.manager_success")
    def save(self):
        manager_data = simplejson.loads(request.body)
        self.company_service.update_manager(self.company_id, **manager_data)
        return dict()


class ShareholderController(BaseController):
    def __init__(self, id):
        self.company_id = int(id)
        self.repository = CompanyRepository(DBSession)
        self.company_shareholder = CompanyShareholder(self.company_id, self.repository)

    @expose()
    def list(self):
        self.shareholders = self.company_shareholder.all_shareholders()
        shareholders_list = list(ShareholderPresenter(shareholder)
                                 for shareholder in self.shareholders)
        return jsonpickle.encode(shareholders_list)

    @expose()
    def save(self):
        submitted_data = simplejson.loads(request.body)
        if submitted_data.get("id"):
            return jsonpickle.encode(self.company_shareholder.
                                     save_shareholder_by_id(submitted_data["id"],
                                     **submitted_data))
        else:
            return jsonpickle.encode(self.company_shareholder.
                                     add_shareholder(**submitted_data))


class DateLetterSentController(BaseController):
    def __init__(self, id):
        self.company_id = int(id)
        self.repository = CompanyRepository(DBSession)

    @expose()
    def save(self):
        submitted_data = simplejson.loads(request.body)
        date_letter_sent = datetime.strptime(submitted_data["date_sent_letter_of_acceptance"],
                                             "%d/%m/%Y")
        company = self.repository.get(self.company_id)
        company.date_sent_letter_of_acceptance = date_letter_sent
        company = self.repository.persist(company)
        company_presenter = CompanyDetailsPresenter(company)
        return jsonpickle.encode(company_presenter)


class AnnualReportController(BaseController):
    def __init__(self, id):
        self.company_id = int(id)
        self.repository = CompanyRepository(DBSession)

    @expose()
    def save(self):
        submitted_data = simplejson.loads(request.body)
        annual_report = add_annual_report_for_company(self.company_id,
                                                      self.repository,
                                                      **submitted_data)
        return jsonpickle.encode(AnnualReportPresenter(annual_report))

    @expose()
    def list(self):
        annual_reports = self.repository.annual_reports_for_company(self.company_id)
        reports = [AnnualReportPresenter(report) for report in annual_reports]
        return jsonpickle.encode(reports)
