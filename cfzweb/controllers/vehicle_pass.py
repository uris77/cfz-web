from tg import expose, request
from cfzweb.lib.base import BaseController

from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository
from cfzweb.presenters.company import (CompanySummaryPresenter)
from cfzweb.model import DBSession
from cfzcore.interactors.vehicle_pass import (
    create_vehicle_pass,
    fetch_all_vehicle_passes)

import simplejson
import jsonpickle


class VehiclePassController(BaseController):

    @expose()
    def index(self, **kwargs):
        method = request.environ['REQUEST_METHOD']

        if method == 'POST':
            submitted_data = simplejson.loads(request.body)
            vehicle_pass = self._create(**submitted_data)
            return jsonpickle.encode(vehicle_pass)

    @expose('json')
    def list(self):
        repository = VehiclePassRepository(DBSession)
        passes = [vehiclepass.__tuple__ for vehiclepass in fetch_all_vehicle_passes(repository)]
        return jsonpickle.encode(passes)

    @expose('cfzweb.templates.vehicle_pass.index')
    def show(self):
        return dict(page='index')

    @expose()
    def create(self):
        repository = CompanyRepository(DBSession)
        companies = repository.all_authorized_companies()
        companies_list = list(CompanySummaryPresenter(company).__dict__ for
                              company in companies)
        return dict(companies=companies_list)

    def _create(self, **kwargs):
        repository = VehiclePassRepository(DBSession)
        vehicle_pass = create_vehicle_pass(repository, **kwargs)
        return vehicle_pass
