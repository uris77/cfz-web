from tg.controllers import DecoratedController
from httplib import HTTPException


class RoutingController(DecoratedController):
    def __perform_call(self, func, args):
        if not args:
            args = {}

        try:
            aname = str(args.get('action', 'lookup'))
            controller = getattr(self, aname)
            print "\ncalling controller: ", controller

            func_name = func.__name__
            if func_name == '__before__' or func_name == '__after__':
                if func_name == '__before__' and hasattr(controller.im_class, '__before__'):
                    return controller.im_self.__before__(*args)
                if func_name == '__after__' and hasattr(controller.im_class, '__after__'):
                    return controller.im_self.__after__(*args)
                return

            else:
                controller = func
                params = args
                remainder = ''

                print "\nCalling controller: ", controller
                print "with params: ", params
                result = DecoratedController._perform_call(
                    self, controller, params, remainder=remainder)

        except HTTPException, httpe:
            result = httpe
            # 304 Not Modified's shouldn't have a content-type set
            if result.status_int == 304:
                result.headers.pop('Content-Type', None)
            result._exception = True
        return result
