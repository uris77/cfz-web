from tg import expose, redirect, request
from tg.decorators import override_template

from cfzweb.lib.base import BaseController
from cfzweb.presenters import ApplicationPresenter, PersonPresenter
from cfzweb.presenters import InvestorPresenter
from cfzweb.presenters.errors import FormIncompleteErrorPresenter
from cfzweb.controllers.investor import InvestorController
from cfzweb.model import DBSession

import jsonpickle
import simplejson
from collections import namedtuple

from cfzcore.company import CompanyType
from cfzcore.interactors.application_builder import ApplicationFormBuilder
from cfzcore.interactors.application import ApplicationBasicInformation
from cfzcore.interactors.application import ApplicationInvestorForm
from cfzcore.interactors.application_builder import FormIncompleteException
from cfzcore.approvers import InvestorBackgroundCheckEmptyException, NoInvestorsException
from cfzcore.approvers import InvestorBackgroundCheckFailed
from cfzweb.services import application_approver
from cfzpersistence.repository.application import ApplicationRepository


class ApplicationController(BaseController):
    def __init__(self):
        self.application_repository = ApplicationRepository(DBSession)
        self.builder = ApplicationFormBuilder(self.application_repository)

    @expose()
    def _lookup(self, id, sub_path, *args):
        investor = InvestorController()
        return investor, args

    @expose()
    def index(self):
        redirect("/application/list")

    @expose('cfzweb.templates.application.create_application_form')
    def create(self, *args, **kw):
        company_types = ["Import/Export", "Manufacturing"]
        return dict(page="create_application_form", company_types=company_types)

    @expose()
    def create_application(self, **kwargs):
        print "\nkwargs: ", kwargs
        application = self.builder.create_application(**kwargs)
        return redirect("show/%s" % application.id)

    @expose('cfzweb.templates.application.list')
    def list(self):
        applications = self.application_repository.all_with_status_new()
        return dict(page='list', applications=applications)

    @expose('cfzweb.templates.application.pending_applications')
    def pending_applications(self):
        applications = self.application_repository.all_with_status_pending()
        return dict(page="pending_applications", applications=applications)

    @expose()
    def pending_list(self):
        applications = self.application_repository.all_with_status_pending()
        application_list = list(ApplicationPresenter(application.__tuple__) for application in applications)
        return jsonpickle.encode(application_list)

    @expose('cfzweb.templates.application.show')
    def show(self, id):
        return dict(page="show", id=id)

    @expose()
    def update_general_information(self):
        submitted_data = simplejson.loads(request.body)
        try:
            form_builder = ApplicationBasicInformation(self.application_repository)
            submitted_data["application_id"] = submitted_data["id"]
            form_builder.update_basic_information(**submitted_data)
            redirect("general_information_success")
        except FormIncompleteException, (exception):
            override_template(self.update_general_information, "jinja:cfzweb.templates.application.general_information_error")
            errors = FormIncompleteErrorPresenter(exception.error)
            return dict(errors=errors)

    @expose()
    def update_consultant(self):
        '''
        Update a company's consultant information.This method receives the request as
        json, parses it, updates the database and returns a representation of the
        consultant as json.
        '''
        submitted_data = simplejson.loads(request.body)
        consultant = self.builder.save_consultant_for(submitted_data["id"], **submitted_data)
        presenter = PersonPresenter(consultant)
        person_json = jsonpickle.encode(presenter, unpicklable=False)
        return person_json

    @expose()
    def update_manager(self):
        '''
        Update a company's manager information.This method receives the request as
        json, parses it, updates the database and returns a representation of the
        manager as json.
        '''
        submitted_data = simplejson.loads(request.body)
        manager = self.builder.update_manager_for(submitted_data["id"], **submitted_data)
        presenter = PersonPresenter(manager)
        person_json = jsonpickle.encode(presenter, unpicklable=False)
        return person_json

    @expose()
    def update_address(self):
        '''
        Update's the location of origin for the company that submitted
        an application.
        '''
        submitted_data = simplejson.loads(request.body)
        basic_information_form = ApplicationBasicInformation(self.application_repository)
        submitted_data["application_id"] = submitted_data["id"]
        application = basic_information_form.save_company_location(**submitted_data)
        presenter = ApplicationPresenter(application)
        return jsonpickle.encode(presenter, unpicklable=False)

    @expose('json')
    def get(self, id):
        application = self.application_repository.get(int(id))
        presenter = ApplicationPresenter(application)
        json_data = jsonpickle.encode(presenter, unpicklable=False)
        return json_data

    @expose()
    def update_contact_information(self):
        '''
        Update the contact information for a company that submitted
        an application form.
        '''
        submitted_data = simplejson.loads(request.body)
        basic_information_form = ApplicationBasicInformation(self.application_repository)
        submitted_data["application_id"] = submitted_data["id"]
        application = basic_information_form.save_contact_numbers(**submitted_data)
        presenter = ApplicationPresenter(application)
        json_data = jsonpickle.encode(presenter, unpicklable=False)
        return json_data

    def __get_company_type__(self, company_type_str):
        company_type = None
        if company_type_str == 'IMPORT/EXPORT':
            company_type = CompanyType.IMPORT_EXPORT
        elif company_type_str == 'MANUFACTURING':
            company_type = CompanyType.MANUFACTURING
        else:
            company_type = None
        return company_type

    @expose('cfzweb.templates.application.general_information_success')
    def general_information_success(self):
        return dict(page="general_information_success")

    @expose()
    def investors(self, id):
        investor_form = ApplicationInvestorForm(self.application_repository)
        _investors = investor_form.get_all_investors_for(int(id))
        investors = []
        for investor in _investors:
            investors.append(InvestorPresenter(investor))
        investors_json = jsonpickle.encode(investors, unpicklable=False)
        return investors_json

    @expose()
    def add_investor(self):
        submitted_data = simplejson.loads(request.body)
        investor_form = ApplicationInvestorForm(self.application_repository)
        Investor = namedtuple("Investor", "first_name last_name percent_owned")
        investor = Investor(first_name=submitted_data['first_name'],
                            last_name=submitted_data['last_name'],
                            percent_owned=submitted_data['percent_owned'])
        investor_form.add_investor_to(int(submitted_data["id"]), investor)
        return redirect("investors/%s" % submitted_data["id"])

    @expose()
    def remove_investor(self):
        submitted_data = simplejson.loads(request.body)
        investor_form = ApplicationInvestorForm(self.application_repository)
        investor_form.remove_investor_from(submitted_data["application_id"],
                                           submitted_data["id"])
        return dict()

    @expose()
    def save_description_of_goods(self):
        submitted_data = simplejson.loads(request.body)
        submitted_data["application_id"] = submitted_data["id"]
        basic_information_form = ApplicationBasicInformation(self.application_repository)
        basic_information_form.save_description_of_goods(**submitted_data)
        return dict()

    @expose()
    def save_background_check(self):
        investor_form = ApplicationInvestorForm(self.application_repository)
        submitted_data = simplejson.loads(request.body)
        submitted_data["investor_id"] = submitted_data["id"]
        investor_form.add_background_check(**submitted_data)
        return dict()

    @expose("cfzweb.templates.application.approve_success_message")
    def approve(self, id):
        """Process request for approving an application"""
        try:
            application = application_approver.approve(id)
            return dict(company=application.company.name)
        except InvestorBackgroundCheckEmptyException, (exception):
            override_template(self.approve, "jinja:cfzweb.templates.application.approve_error_message")
            return dict(investors=exception.error.investors)
        except InvestorBackgroundCheckFailed, (exception):
            override_template(self.approve, "jinja:cfzweb.templates.application.investor_failed_background_check_message")
            return dict(shareholder=exception.error)
        except NoInvestorsException, (exception):
            override_template(self.approve, "jinja:cfzweb.templates.application.approve_no_investors_error_message")
            return dict()

    @expose('cfzweb.templates.application.denied_list')
    def denied(self):
        applications = self.application_repository.all_denied_applications()
        return dict(page="denied_list", applications=applications)

    @expose("cfzweb.templates.application.denied_message")
    def deny(self):
        '''Deny an application'''
        submitted_data = simplejson.loads(request.body)
        application_id = submitted_data['id']
        reason_for_denial = submitted_data['reason_for_denial']
        return dict(company=application_approver.deny(application_id, reason_for_denial))
