from tw2.core import Required
import tw2.bootstrap.forms as forms

class ApplicationForm(forms.Form):
    class child(forms.HorizontalForm):
        name = forms.TextField(validator=Required)
        first_name = forms.TextField(validator=Required)
        last_name = forms.TextField(validator=Required)
        registration_number = forms.TextField(validator=Required)

    action = 'create'

