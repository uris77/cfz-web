require.config({
   paths: {
      bootstrap:  'bootstrap.min',
      jquery: 'jquery-1.7.2.min',
      jqueryui: 'libs/jquery-ui-1.8.21.custom.min',
      lodash: 'libs/lodash.min',
      backbone: 'libs/backbone.min',
      underscore: 'libs/underscore',
      templates: '../templates'
   },
   shim: {
      'bootstrap': ['jquery'],
      'jqueryui': ['jquery'],
      'backbone': ['lodash', 'jquery']
   }
});

require(['app'], function(App){
   App.initialize();
});
