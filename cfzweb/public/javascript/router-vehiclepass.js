define(
   [
      'jquery',
      'lodash',
      'backbone',
      'app/vehicle_pass/vehiclepass_list_view',
      'app/vehicle_pass/vehiclepass_create_view'
], function($,_, backbone, vehiclePassListView, vehiclePassCreateView){
      var AppRouter = Backbone.Router.extend({
         routes: {
            "list": "vehiclePassListView",
            "create": "showVehiclePassForm",
            //Defaults
            "*actions": "defaultAction"
         },

         showVehiclePassForm: function(){
            vehiclePassCreateView.render();
         },

         vehiclePassListView: function(){
            vehiclePassListView.render();
         },

         defaultAction: function(actions){
            alert("Default");
         }

      });

      var initialize = function(){
         var app_router = new AppRouter();
         Backbone.history.start();
      };

      return{
         initialize: initialize
      };
});

