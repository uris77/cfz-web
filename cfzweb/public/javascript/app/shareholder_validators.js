angular.module('shareholderValidators', ['ngResource']).
   factory("shareholderValidator", function(){
      function ShareholderValidator(){

         this.validate_names =  function(shareholder, shareholders){
                                 var self = this;
                                 self.valid_name = true;
                                 $.each(shareholders, function(index, _shareholder){
                                    if(has_this_first_name(shareholder, _shareholder) && has_this_last_name(shareholder, _shareholder) &&
                                       shareholder.id != _shareholder.id){
                                       self.valid_name = false;
                                       return false;
                                    }else{
                                       self.valid_name = true;
                                    }
                                 });

                                 return self.valid_name;
                              };

         function has_this_first_name(shareholder_to_check, shareholder_in_list){
                  if(shareholder_to_check.first_name != undefined && 
                        shareholder_to_check.first_name.toLowerCase().localeCompare(shareholder_in_list.first_name.toLowerCase()) == 0){
                     return true;
                  }else{
                     return false;
                  }
         };

         function has_this_last_name(shareholder_to_check, shareholder_in_list){
               if(shareholder_to_check.last_name != undefined && 
                     shareholder_to_check.last_name.toLowerCase().localeCompare(shareholder_in_list.last_name.toLowerCase()) == 0){
                        return true;
               }else{
                  return false;
               }
         };

         this.validate_shares = function(shareholder){
                                 if(shareholder.shares != undefined && shareholder.shares > 0){
                                    return true;
                                 }else{
                                    return false;
                                 }
                              };

      }

      return new ShareholderValidator();
})
