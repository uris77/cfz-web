define([
       'jquery',
       'lodash',
       'backbone',
       'app/vehicle_pass/vehiclepass-model',
], function($, _, backbone, VehiclePass){
      var VehiclepassCollection = Backbone.Collection.extend({
      model: VehiclePass,
      url: '/vehicle_pass/list',
      initialize: function(){
         console.log("initialized vehicle passes collections");
      }
   });

   return new VehiclepassCollection;
});

