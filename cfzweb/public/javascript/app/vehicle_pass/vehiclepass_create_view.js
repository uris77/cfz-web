define([
       'jquery',
       'lodash',
       'jqueryui',
       'backbone',
       'app/vehicle_pass/vehiclepass-model',
       'text!templates/vehiclepass/create.html'
], function($, _, jqui, backbone, VehiclePass, vehiclepassCreateTemplate){
   var vehiclePassCreateView = Backbone.View.extend({
      el: $("#mainpanel"),
      model: VehiclePass,
      initialize: function(){
      },

      showError: function(model, error){
         alert("error");
      },
      events: {
         'submit #vehicleform': 'submit'
      },
      submit: function(e){
         e.preventDefault();
         vehiclePass = new VehiclePass();
         vehiclePass.set(
            {'owner':            $('input[name=owner]').val(),
             'company_id':       $('select option:selected').val(),
             'vehicle_year':     $('input[name=vehicle_year]').val(),
             'vehicle_model':    $('input[name=vehicle_model]').val(),
             'color':            $('input[name=color]').val(),
             'license_plate':    $('input[name=license_plate]').val(),
             'sticker_number':   $('input[name=sticker_number]').val(),
             'date_issued':      $('input[name=date_issued]').val(),
             'month_expired':    $('input[name=month_expired]').val(),
             'receipt_number':   $('input[name=receipt_number]').val()
            }
         );
         console.log("submitted vehiclepass: " + JSON.stringify(vehiclePass));
         vehiclePass.save();
         //vehiclePass.bind('error', this.showError, this);
      },

      render: function(){
         var compiledTemplate = _.template(vehiclepassCreateTemplate);
         $.get("/company/selectbox", function(data){
            $("#companies-selectbox").empty();
            $("#companies-selectbox").append(data);
         });
         $("#mainpanel").html(compiledTemplate);
         $("#date_issued").datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
         });
      }
   });

   return new vehiclePassCreateView();
});
