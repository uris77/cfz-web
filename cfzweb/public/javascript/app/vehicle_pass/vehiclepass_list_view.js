define([
       'jquery',
       'lodash',
       'backbone',
       'app/vehicle_pass/vehiclepass_collections',
       'text!templates/vehiclepass/list.html'
], function($, _, backbone, VehiclepassCollection, vehiclepassListTemplate){
      var VehiclepassListView = Backbone.View.extend({
         initialize: function(){
            console.log("initializing vehicle pass view list....");
            this.vehiclepasses = VehiclepassCollection;
         },
         render: function(){
            console.log("rendering vehicle pass view list....");
            this.vehiclepasses.fetch({success: function(passes){
               var data = {
                  vehiclepasses: passes.models,
                  _: _
               };
               var compiledTemplate = _.template(vehiclepassListTemplate, data);
               $("#mainpanel").html(compiledTemplate);
            }});
         }
      });

      return new VehiclepassListView();
});
