angular.module("companyService", ['ngResource']).
   factory("companyService", function($http, $rootScope){
      function CompanyService(){

         this.get_company = function(id){
            fetch_company(id);
         };

         function fetch_company(id){
            $http.get(CFZ.Url.Path.for('company/fetch/'+id)).
               success(function(company){
                  $rootScope.$broadcast("company_data_retrieved", company);
               });
         };

         this.get_shareholders = function(company_id){
            fetch_shareholders(company_id);
         };

         function fetch_shareholders(company_id){
            $http.get(CFZ.Url.Path.for('company/'+company_id+'/shareholder/list')).
                  success(function(shareholders, status){
                     $rootScope.$broadcast("shareholders_list_retrieved", shareholders);
            });
         };

         this.save_date_sent_letter_of_acceptance = function(company_id, jsonData){
            post_date_sent_letter_of_acceptance(company_id, jsonData);
         };

         function post_date_sent_letter_of_acceptance(company_id, jsonData){
            $http.post(CFZ.Url.Path.for('company/'+company_id+'/date_letter_sent/save'), jsonData).
                  success(function(company, status){
                     $rootScope.$broadcast("company_data_retrieved", company);
            });
         };

         this.save_shareholder = function(company_id, jsonData){
            post_shareholder(company_id, jsonData);
         };

         function post_shareholder(company_id, jsonData){
            $http.post(CFZ.Url.Path.for('company/'+company_id+'/shareholder/save'), jsonData).
                  success(function(shareholder, status){
                     shareholder.first_name = shareholder.person.first_name;
                     shareholder.last_name = shareholder.person.last_name;
                     $rootScope.$broadcast('shareholder_saved', shareholder);
            });
         };

         this.update_manager = function(company_id, jsonData){
            post_manager(company_id, jsonData);
         };

         function post_manager(company_id, jsonData){
            $http.post(CFZ.Url.Path.for('company/'+company_id+'/manager/save'), jsonData).
                  success(function(data, status){
                     $rootScope.$broadcast("manager_updated", data);
            });
         };

         this.update_contact_numbers = function(company_id, jsonData){
            post_contact_numbers(company_id, jsonData);
         };

         function post_contact_numbers(company_id, jsonData){
            $http.post(CFZ.Url.Path.for('company/'+company_id+'/contact_numbers/save'), jsonData).
                  success(function(data, statu){
                     $rootScope.$broadcast("contact_numbers_updated", data);
            });
         };

         this.update_address = function(company_id, jsonData){
            post_address(company_id, jsonData);
         };

         function post_address(company_id, jsonData){
            $http.post(CFZ.Url.Path.for('company/'+company_id+'/address/save'), jsonData).
                  success(function(data, status){
                     $rootScope.$broadcast("address_updated", data);
            });
         };

         this.save_annual_report = function(company_id, jsonData){
            post_annual_report(company_id, jsonData);
         };

         function post_annual_report(company_id, jsonData){
            $http.post(CFZ.Url.Path.for('company/'+company_id+'/annual_report/save'), jsonData).
                  success(function(report, status){
                     $rootScope.$broadcast("annual_report_saved", report);
            });
         };

         this.get_annual_reports = function(company_id){
            fetch_annual_reports(company_id);
         };

         function fetch_annual_reports(compan_id){
            $http.get(CFZ.Url.Path.for('company/'+company_id+'/annual_report/list')).
                  success(function(reports, status){
                     $rootScope.$broadcast("annual_reports_retrieved", reports);
                  });
         };

      }

      return new CompanyService();
})
