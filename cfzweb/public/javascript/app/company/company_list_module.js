var company_list_module = angular.module("company_list_module", ["ngResource"]);

company_list_module.config(function($routeProvider){
   $routeProvider.
      when('/', {templateUrl: CFZ.Url.Path.for('partials/company/list.html')}).
      otherwise({redirectTo: '/', templateUrl: CFZ.Url.Path.for('partials/company/list.html')});
});

company_list_module.service('company_list_service', function($rootScope, $http){

   var self = this;

   self.all_companies = {};

   this.get_companies = function(){
      $http({method:"GET", url:CFZ.Url.Path.for('company/list')}).
            success(function(companies, status){
               self.all_companies = angular.copy(companies);
               $rootScope.$broadcast('companies_retrieved', companies);
            });
   }


   this.search= function(company_name){
      if(company_name === ""){
         $rootScope.$broadcast('searching_companies', self.all_companies);
      }else{
         $http({method:"GET", url:CFZ.Url.Path.for('company/search_with_name/'+company_name)}).
               success(function(data, status){
                  $rootScope.$broadcast('searching_companies', data);
               });
      }
   }

});

function CompanyListController($scope, $http, company_list_service){
   $scope.companies = [];
   $scope.all_companies = {};

   $scope.$on("companies_retrieved", function(event, data){
      $scope.companies = angular.copy(data);
      $scope.all_companies = angular.copy(data);
   });

   $scope.$on("searching_companies", function(event, data){
      $scope.companies = angular.copy(data);
   });

   company_list_service.get_companies();
}

function SearchController($scope, $http, company_list_service){
   $scope.companies = [];
   $scope.all_companies = {};

   $scope.search = function(){
      company_list_service.search(this.company_name);
   }

   $scope.$on("companies_retrieved", function(event, data){
      $scope.companies = angular.copy(data);
      $scope.all_companies = angular.copy(data);
   });
}

