var show_company_module = angular.module("show_company_module", ["ngResource", "shareholderValidators", "companyService", "ui.directives"]);

show_company_module.config(function($locationProvider, $routeProvider){
   $routeProvider.
      when('/general', {templateUrl: CFZ.Url.Path.for('partials/company/general_information.html')}).
      when('/contact_numbers', {templateUrl: CFZ.Url.Path.for('partials/company/contact_numbers.html')}).
      when('/address', {templateUrl: CFZ.Url.Path.for('partials/company/address.html')}).
      when('/manager', {templateUrl: CFZ.Url.Path.for('partials/company/manager.html')}).
      when('/reports', {templateUrl: CFZ.Url.Path.for('partials/company/annual_reports.html')}).
      otherwise({redirectTo: '/general', template: CFZ.Url.Path.for('partials/company/general_information.html')});
      
   $routeProvider.
      when('/investors', {
         templateUrl: CFZ.Url.Path.for('partials/company/investors.html'),
         });
});

show_company_module.service("shares_validator", function($rootScope, $http){
   var self = this;

   self.is_valid_share = function(shares_owned, old_shares, shareholder){
      if(shareholder.shares >= 0){
         if((shares_owned + shareholder.shares - old_shares) > 100){
            $("#shares_message").empty();
            $("#shares_message").append("<span class='alert alert-error'><i class='icon-ban-circle'></i>&nbsp; Invalid shares!</span>");
         }else{
            $("#shares_message").empty();
            $("#shares_message").append("<span class='alert alert-success'><i class='icon-ok-circle'></i></span>");
         }
      }
   }

})

show_company_module.controller("GeneralInformationController", 
                               ['$scope','$location', 'shares_validator', 'shareholderValidator', 'companyService',
                               function($scope, $location, shares_validator, shareholderValidator, companyService){

   $scope.company = {};
   $scope.shareholders = {};
   $scope.shareholder = {};
   $scope.annual_reports = {};
   $scope.annual_report = {};
   $scope.add_shareholder_button = true;
   $scope.show_shareholder_form = false;
   $scope.show_acceptance_letter_form = false;
   $scope.add_report_button = true;
   $scope.is_report_form_on = false;
   $scope.date_sent_letter_of_acceptance = new Date();
   $scope.dateOptions = {
      changeYear: true,
      changeMonth: true,
      yearRange: '2000:-0',
      dateFormat: 'dd/mm/yy'
   }

   $scope.get_company = function(){
      var path = window.location.pathname.split("/");
      this.company_id = path[path.length - 1];
      companyService.get_company(this.company_id);
   }

   $scope.$on("company_data_retrieved", function(event, data){
      $scope.company = angular.copy(data);
      if($scope.company.date_sent_letter_of_acceptance == null){
         $scope.show_acceptance_letter_form = true;
      }else{
         $scope.show_acceptance_letter_form = false;
      }
   });

   $scope.set_route = function(route){

      $location.path(route);

      switch(route)
      {
         case "general":
            $("#nav-items li").toggleClass("active", false);
            $("#general-li").toggleClass("active", true);
            break;
         case "contact_numbers":
            $("#nav-items li").toggleClass("active", false);
            $("#contact-numbers-li").toggleClass("active", true);
            break;
         case "address":
            $("#nav-items li").toggleClass("active", false);
            $("#address-li").toggleClass("active", true);
            break;
         case "manager":
            $("#nav-items li").toggleClass("active", false);
            $("#manager-li").toggleClass("active", true);
            break;
         case "investors":
            $("#nav-items li").toggleClass("active", false);
            $("#investors-li").toggleClass("active", true);
            break;
         case "reports":
            $("#nav-items li").toggleClass("active", false);
            $("#reports-li").toggleClass("active", true);
            break;
      }

      var $this = $(this);
   }

   $scope.update_address = function(){
      var jsonData = {'id': $scope.company.id,
                      'state': $scope.company.state,
                      'country': $scope.company.country};
      companyService.update_address($scope.company.id, jsonData);
   }

   $scope.$on("address_updated", function(event, data){
      $("#address_message").empty();
      $("#address_message").append(data);
   });

   $scope.update_contact_numbers = function(){
      var jsonData = {"id": $scope.company.id,
                      "telephone_number": $scope.company.telephone_number,
                      "cable_number": $scope.company.cable_number,
                      "fax_number": $scope.company.fax_number};
      companyService.update_contact_numbers($scope.company.id, jsonData);
   }

   $scope.$on("contact_numbers_updated", function(event, data){
      $("#contact_numbers_message").empty();
      $("#contact_numbers_message").append(data);
   });

   $scope.update_manager = function(){
      var jsonData = {"id": $scope.company.id,
                      "first_name": $scope.company.manager_first_name,
                      "last_name": $scope.company.manager_last_name};
      companyService.update_manager($scope.company.id, jsonData);
   }

   $scope.$on("manager_updated", function(event, data){
      $("#manager_message").empty();
      $("#manager_message").append(data);
   });

   $scope.validate_shareholder_names = function(){
      return shareholderValidator.validate_names($scope.shareholder, $scope.shareholders);
   }

   $scope.validate_shares = function(shareholder){
      return shareholderValidator.validate_shares(shareholder);
   }

   function indexOfShareholder(shareholders, shareholder){
      var index = -1;
      $.each(shareholders, function(_index, _shareholder){
         if(_shareholder.id === shareholder.id){
            index =  _index;
            return false;
         }
      });
      return index;
   };

   $scope.save_shareholder = function(){
      if($scope.validate_shareholder_names() && $scope.validate_shares($scope.shareholder)){
         var jsonData = {"id": $scope.shareholder.id,
                         "first_name": $scope.shareholder.first_name,
                         "last_name": $scope.shareholder.last_name,
                         "shares": $scope.shareholder.shares,
                         "company_id": $scope.company.id};
         companyService.save_shareholder($scope.company.id, jsonData);
      }
   }

   $scope.$on("shareholder_saved", function(event, shareholder){
      $("#message").empty();
      $("#message").append("<div class='alert alert-info alert-block'>Shareholder saved!</div>")
      var shareholder_index = indexOfShareholder($scope.shareholders, $scope.shareholder);
      if(shareholder_index>0){
         $scope.shareholders.splice(shareholder_index, 1);
      }
      $scope.shareholders.push(shareholder);
      $scope.hide_shareholder_form();
   });

   $scope.old_shares_for = function(shareholder){
      self.old_shares = 0;
      $.each($scope.shareholders, function(index, value){
         if(value.id == shareholder.id){
            self.old_shares = value.shares;
            return self.old_shares;
         }
      });
      return self.old_shares;
   }

   $scope.shareholder_form = function(){
      $scope.display_shareholder_form();
   }

   $scope.close_shareholder_form = function(){
      $scope.hide_shareholder_form();
   }

   $scope.edit_shareholder = function(shareholder){
      $scope.display_shareholder_form();
      $scope.shareholder = angular.copy(shareholder);
   }

   $scope.display_shareholder_form = function(){
      $("#message").empty();
      $scope.show_shareholder_form = true;
      $scope.add_shareholder_button = false;
   }

   $scope.hide_shareholder_form = function(){
      $scope.show_shareholder_form = false;
      $scope.add_shareholder_button = true;
      $scope.shareholder = {}
   }

   $scope.get_shareholders = function(){
      var path = window.location.pathname.split("/");
      var company_id = path[path.length - 1];
      companyService.get_shareholders(company_id);
   }

   $scope.save_date_sent_letter_of_acceptance = function(){
      var month = $scope.date_sent_letter_of_acceptance.getMonth() + 1;
      var date = $scope.date_sent_letter_of_acceptance.getDate();
      var year = $scope.date_sent_letter_of_acceptance.getFullYear();
      $scope.date_sent_letter_of_acceptance = date + "/" + month + "/" + year;
      var jsonData = {'date_sent_letter_of_acceptance': $scope.date_sent_letter_of_acceptance};
      companyService.save_date_sent_letter_of_acceptance($scope.company.id, jsonData);
      $scope.show_acceptance_letter_form = false;
   }

   $scope.$on("shareholders_list_retrieved", function(event, shareholders){
      $scope.shareholders = angular.copy(shareholders);
   });

   $scope.show_report_form = function(){
      $scope.is_report_form_on = true;
      $scope.add_report_button = false;
      $("#message").empty();
   }

   $scope.close_report_form = function(){
      $scope.is_report_form_on = false;
      $scope.add_report_button = true;
      $scope.annual_report = {};
   }

   $scope.save_report = function(){
      var jsonData = {'date_submitted': date_to_string($scope.annual_report.date_submitted),
                      'start_date': date_to_string($scope.annual_report.start_date),
                      'end_date': date_to_string($scope.annual_report.end_date)}
      companyService.save_annual_report($scope.company_id, jsonData);
   }

   $scope.$on("annual_report_saved", function(event, report){
      var report_index = indexOfAnnualReport($scope.annual_reports, report);
      if(report_index>0){
         $scope.annual_reports.splice(report_index, 1);
      }
      $scope.annual_reports.push(report);
      $scope.close_report_form();
      $("#message").empty();
      $("#message").append("<div class='alert alert-info alert-block'>Annual Report Saved!</div>");
   });

   function indexOfAnnualReport(annual_reports, report){
      var index = -1;
      $.each(annual_reports, function(_index, _report){
         if(_report.id === report.id){
            index =  _index;
            return false;
         }
      });
      return index;
   };

   function date_to_string(_date){
      var date = _date.getDate();
      var month = _date.getMonth() + 1;
      var year = _date.getFullYear();

      return date + "/" + month + "/" + year;
   };

   function get_annual_reports(){
      companyService.get_annual_reports(company_id());
   };

   $scope.$on("annual_reports_retrieved", function(event, reports){
      $scope.annual_reports = angular.copy(reports);
   });

   function company_id(){
      var path = window.location.pathname.split("/");
      this.company_id = path[path.length - 1];
      return this.company_id;
   };

   $scope.get_company();
   $scope.get_shareholders();
   get_annual_reports();

}]);

