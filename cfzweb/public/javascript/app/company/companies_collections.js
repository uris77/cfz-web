define([
       'jquery',
       'lodash',
       'backbone',
       'app/company/company-model',
], function($, _, backbone, companyModel){
      var companiesCollection = Backbone.Collection.extend({
      model: companyModel,
      url: '/company/list',
      initialize: function(){
         console.log("initialized company collections");
      }
   });

   return new companiesCollection;
});
