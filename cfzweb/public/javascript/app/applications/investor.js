application.service('applicationInvestorService', function($rootScope, $http){
   this.investors = {};

   this.getInvestors = function(id){
      $http({method:'GET', url:CFZ.Url.Path.for('application/investors/'+id)}).
            success(function(data,status){
               this.investors = angular.copy(data);
               $rootScope.$broadcast('investorsRetrieved', data);
      });
   }

   this.addInvestor = function(jsonData){
      $http({method:'POST', url:CFZ.Url.Path.for('application/add_investor'), data:jsonData}).
            success(function(data, status){
            this.investors = angular.copy(data);
            $rootScope.$broadcast('investorsRetrieved', data);
      });
   }

   this.removeInvestor = function(jsonData){
      $http({method:'POST', url:CFZ.Url.Path.for('application/remove_investor'), data:jsonData});
   }

   this.addBackgroundCheck =  function(jsonData){
      $http({method:'POST', url:CFZ.Url.Path.for('application/save_background_check'), data:jsonData});
   }

   this.passBackgroundCheck = function(application_id, investor_id){
      $http({method:'POST', url:CFZ.Url.Path.for('application/'+application_id+'/investor/passed'), data:{'id': investor_id}});
   }

   this.failBackgroundCheck = function(application_id, investor_id){
      $http({method:'POST', url:CFZ.Url.Path.for('application/'+application_id+'/investor/failed'), data:{'id': investor_id}});
   }

});

function InvestorApplicationController($scope, $http, applicationInvestorService, shareholderValidator){

   var self = this;
   self.investors = {};
   $scope.investors = {};
   $scope.successMessage = false;
   $scope.show_delete_modal=false;
   $scope.investor_deleted_message = false;
   $scope.show_background_modal=false;
   $scope.shareholder = {};

   $scope.$on('investorsRetrieved', function(event, investors){
      $scope.investors = angular.copy(investors)
   });

   $scope.add_investor = function(){
      var jsonData = {'id': $scope.applicationId,
         'first_name': $scope.shareholder.first_name,
         'last_name': $scope.shareholder.last_name,
         'percent_owned': $scope.shareholder.percent_owned};
      if($scope.validate_shareholder_name() == true){
         applicationInvestorService.addInvestor(jsonData);
         $scope.clear_form();
         $scope.successMessage = true;
         $scope.investor_deleted_message = false;
      }
   }

   $scope.validate_shareholder_name = function(){
      console.log("validating name: " + shareholderValidator.validate_names($scope.shareholder, $scope.investors));
      return shareholderValidator.validate_names($scope.shareholder, $scope.investors);
   }

   $scope.investor_to_delete = {};
   $scope.delete_investor = function(investor){
      $scope.investor_to_delete = angular.copy(investor);
      $scope.show_delete_modal = true;
      $scope.investor_deleted_message = false;
      $scope.successMessage = false;
   }

   $scope.confirm_delete_investor = function(){
      var index_of_investor_to_delete = $scope.investors.indexOf($scope.investor_to_delete);
      $.each($scope.investors, function(idx, value){
         if($scope.investor_to_delete.id === value.id){
            index_of_investor_to_delete = idx;
            return false;
         }
      });
      var jsonData = {'application_id': $scope.applicationId, 'id': $scope.investor_to_delete.id};
      applicationInvestorService.removeInvestor(jsonData);
      if(index_of_investor_to_delete!=-1){
         $scope.investors.splice(index_of_investor_to_delete, 1);
         $scope.close_delete_investor_modal();
         $scope.investor_deleted_message = true;
      }
   }

   $scope.close_delete_investor_modal = function(){
      $scope.show_delete_modal = false;
   }

   $scope.show_background_check_form = function(investor){
      $scope.investor = angular.copy(investor);
      $scope.show_background_modal = true;
      $scope.background_check_success = false;
   }

   $scope.save_background_check = function(){
      var jsonData = {'id': $scope.investor.id,'background_check': $scope.investor.background_check}
      applicationInvestorService.addBackgroundCheck(jsonData)
   }

   $scope.pass_background_check = function(){
      $scope.background_check_success = true;
      applicationInvestorService.passBackgroundCheck($scope.applicationId, $scope.investor.id);
      $scope.fetch_investors();
   }

   $scope.fail_background_check = function(){
      $scope.background_check_success = true;
      applicationInvestorService.failBackgroundCheck($scope.applicationId, $scope.investor.id);
      $scope.fetch_investors();
   }

   $scope.close_background_modal = function(){
      $scope.background_check_success = false;
      $scope.show_background_modal = false;
      $scope.fetch_investors();
   }

   $scope.clear_form = function(){
      $scope.shareholder = {};
   }


   $scope.fetch_investors = function(){
      var path = window.location.pathname.split("/");
      $scope.applicationId = path[path.length - 1];
      applicationInvestorService.getInvestors($scope.applicationId);
   }

   $scope.fetch_investors()
}
