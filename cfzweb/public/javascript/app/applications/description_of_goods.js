application.service('goodsService', function($rootScope, $http){

   this.save_description_of_goods = function(jsonData){
      $http({method:'POST', url:CFZ.Url.Path.for('application/save_description_of_goods'), data:jsonData}).
            success(function(data, status){
      });
   }

});

function DescriptionOfGoodsController($scope, $http, applicationDataService, goodsService){
   $scope.successMessage = false;
   $scope.descriptionEmptyMessage = false;
   $scope.description_of_goods = {};
   $scope.application = {};

   $scope.add_description_of_goods = function(){
   }

   $scope.$on('applicationDataRetrieved', function(event, data){
      $scope.application = angular.copy(data);
   });

   $scope.save_description_of_goods = function(){
      var jsonData = {'id': $scope.application.id,
         'description_of_goods': $scope.application.description_of_goods};
      goodsService.save_description_of_goods(jsonData);
      $scope.successMessage = true;
   }

   var path = window.location.pathname.split("/");
   var id = path[path.length - 1];
   applicationDataService.getApplication(id);
}

