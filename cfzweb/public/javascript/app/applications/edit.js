var application = angular.module("application", ["ngResource", "shareholderValidators"]);
application.config(function($routeProvider){
   $routeProvider.
      when('/general', {templateUrl: CFZ.Url.Path.for('partials/application/edit/general.html')}).
      when('/contact-information', {templateUrl: CFZ.Url.Path.for('partials/application/edit/contact.html')}).
      when('/address', {templateUrl: CFZ.Url.Path.for('partials/application/edit/address.html')}).
      when('/manager', {templateUrl: CFZ.Url.Path.for('partials/application/edit/manager.html')}).
      when('/investors', {templateUrl: CFZ.Url.Path.for('partials/application/investors.html')}).
      when('/description_of_goods', {templateUrl: CFZ.Url.Path.for('partials/application/edit/description_of_goods.html'), controller:DescriptionOfGoodsController}).
      when('/consultant', {templateUrl: CFZ.Url.Path.for('partials/application/edit/consultant.html')}).
      otherwise({redirectTo: '/general', template: CFZ.Url.Path.for('partials/application/edit/general.html')});
});

application.service('applicationDataService', function($rootScope, $http){
   var _service = {};

  this.applications = {};

  this.getAllApplications = function(){
     $http({method: 'GET', url: CFZ.Url.Path.for('application/all')}).
           success(function(data, status){
               this.applications = angular.copy(data);
               $rootScope.$broadcast('applications_list');
     });
     return this.applications;
  }

  this.application_data = {};
  this.getApplication = function(id){
        $http({method: 'GET', url:CFZ.Url.Path.for('application/get/'+id)}).
           success(function(data,status){
              this.application_data = angular.copy(data);
              $rootScope.$broadcast('applicationDataRetrieved', data);
           });
  }

  this.incomplete_form_error = {};
  this.save_general_information = function(jsonData){
     $http({method: 'POST', url:CFZ.Url.Path.for('application/update_general_information'), data:jsonData}).
           success(function(data, status){
               $("#general_info_messages").append(data);
            });
  }

  this.save_person = function(jsonData){
     $http({method: 'POST', url:CFZ.Url.Path.for('application/update_manager'), data: jsonData}).
           success(function(data, status){
            console.log("status: " + status);
         });
  }

  this.save_consultant = function(jsonData){
     $http({method: 'POST', url:CFZ.Url.Path.for('application/update_consultant'), data: jsonData}).
           success(function(data, status){
               console.log("status: " + status);
      });
  }

  this.save_address= function(json_data){
     $http({method: 'POST', url:CFZ.Url.Path.for('application/update_address'), data:json_data}).
           success(function(data, status){
               console.log("status " + status);
            });
  }

  this.save_contact_information = function(json_data){
     $http({method: 'POST', url:CFZ.Url.Path.for('application/update_contact_information'), data:json_data}).
           success(function(data, status){
               console.log('status ' + status);
         });
  }


});

function ApplicationMainController($scope, $http, $location, applicationDataService){
   $scope.setRoute = function(route){
      $location.path(route);
      $scope.clear_messages();

      switch(route)
      {
         case "general":
            $("#tabs li").toggleClass("active", false);
            $("#general-li").toggleClass("active",true);
            break;
         case "contact-information":
            $("#tabs li").toggleClass("active", false);
            $("#contact-li").toggleClass("active",true);
            break;
         case "address":
            $("#tabs li").toggleClass("active", false);
            $("#address-li").toggleClass("active",true);
            break;
         case "manager":
            $("#tabs li").toggleClass("active", false);
            $("#manager-li").toggleClass("active",true);
            break;
         case "consultant":
            $("#tabs li").toggleClass("active", false);
            $("#consultant-li").toggleClass("active", true);
            break;
         case "investors":
            $("#tabs li").toggleClass("active", false);
            $("#investors-li").toggleClass("active", true);
            break;
         case "description_of_goods":
            $("#tabs li").toggleClass("active", false);
            $("#description-of-goods-li").toggleClass("active", true);
            break;
      }
      var $this = $(this);
   }

   $scope.company_types = ['Import/Export', 'Manufacturing'];
   $scope.application_status = ['New', 'Pending'];
   $scope.general_information_message = false;
   $scope.edit = function(application){
      $location.path('general');
      $("#tabs li").toggleClass("active", false);
      $("#general-li").toggleClass("active",true);
      console.log("getting application with id: " + application.id );
   }

   var path = window.location.pathname.split("/");
   var id = path[path.length - 1];
   applicationDataService.getApplication(id);
   $scope.application = {};

   $scope.$on('applicationDataRetrieved', function(event, application_data){
      $scope.application = angular.copy(application_data);
   });

   $scope.validate_general_information = function(){
      this.general_form_errors = false;
      $scope.company_type_error = false;
      $scope.registration_number_error = false;

      if(!CFZ.is_present($scope.application.company.company_type)){
         $scope.company_type_error = true;
         $scope.general_form_errors = true;
      }

      if(!CFZ.is_numeric($scope.application.company.registration_number)){
         $scope.registration_number_error = true;
         $scope.general_form_errors = true;
      }
      return this.general_form_errors;
   }

   $scope.update_general_information = function(){
      var errors = $scope.validate_general_information()
      if(errors){
         console.log("errors validating...");
      }else{
         $("#general_info_messages").empty();
         var json_data = {'name': $scope.application.company.name, 'status': $scope.application.status,
                           'company_type': $scope.application.company.company_type, 
                           'registration_number': $scope.application.company.registration_number,
                           'id': $scope.application.id
                        };
         applicationDataService.save_general_information(json_data);
      }
   }

   $scope.update_manager = function(){
      var json_data = {'id': $scope.application.manager.id,
                        'first_name': $scope.application.manager.first_name,
                        'last_name': $scope.application.manager.last_name
                     };
      applicationDataService.save_person(json_data);
      $scope.manager_information_message = true;
   }

   /**
    *Update the consultant's information that submitted the application.
    This is done by sending the server the consultant's information in 
    json format.
    */
   $scope.update_consultant = function(){
      var json_data = {'id': $scope.application.id,
                        'first_name': $scope.application.consultant.first_name,
                        'last_name': $scope.application.consultant.last_name
                     };
      applicationDataService.save_consultant(json_data);
      $scope.consultant_information_message = true;
   }

   /**
    * Update the company's address of origin by sending a 
    * json representation of the address to the server.
    */
   $scope.update_address = function(){
      var json_data = {'id': $scope.application.id,
                        'country': $scope.application.location.country,
                        'state': $scope.application.location.state
                     }
      applicationDataService.save_address(json_data);
      $scope.address_message = true;
                        
   }

   /**
    * Update the company's contact information.
    */
   $scope.update_contact_information = function(){
      var json_data = {'id': $scope.application.id,
                        'telephone_number': $scope.application.company.telephone_number,
                        'fax_number': $scope.application.company.fax_number,
                        'cable_number': $scope.application.company.cable_number
               }
      applicationDataService.save_contact_information(json_data);
      $scope.contact_information_message = true;
   }


   $scope.clear_messages = function(){
      $scope.general_information_message = false;
      $scope.manager_information_message = false;
      $scope.consultant_information_message = false;
      $scope.address_message = false;
      $scope.contact_information_message = false;
      $scope.incomplete_form_message = false;
   }
}

