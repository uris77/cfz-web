class CompanySummaryPresenter(object):
    def __init__(self, company):
        self.id = company.id
        self.name = company.name
        self.manager = company.manager.name()
        self.registration_number = company.registration_number
        self.created_date = company.created_date.strftime("%d/%b/%Y")
        self.telephone_number = company.telephone_number
        self.company_type = company.company_type
        if company.date_sent_letter_of_acceptance:
            self.date_sent_letter_of_acceptance = company.\
                date_sent_letter_of_acceptance.strftime("%d/%b/%Y")
        else:
            self.date_sent_letter_of_acceptance = None


class CompanyDetailsPresenter(object):
    def __init__(self, company):
        self.id = company.id
        self.name = company.name
        self.registration_number = company.registration_number
        self.created_date = company.created_date.strftime("%d/%b/%Y")
        self.telephone_number = company.telephone_number
        self.fax_number = company.fax_number
        self.cable_number = company.cable_number
        self.company_type = company.company_type
        self.state = company.state
        self.country = company.country
        self.manager = company.manager.first_name + " " + company.manager.last_name
        self.manager_first_name = company.manager.first_name
        self.manager_last_name = company.manager.last_name
        self.description_of_goods = company.description_of_goods
        self.date_sent_letter_of_acceptance = company.date_sent_letter_of_acceptance
        if self.date_sent_letter_of_acceptance:
            self.date_sent_letter_of_acceptance = self.\
                date_sent_letter_of_acceptance.strftime("%d/%b/%Y")


class AnnualReportPresenter(object):
    def __init__(self, report):
        self.id = report.id
        self.date_submitted = report.date_submitted.strftime("%d/%b/%Y")
        self.start_date = report.start_date.strftime("%d/%b/%Y")
        self.end_date = report.end_date.strftime("%d/%b/%Y")
