class ApplicationPresenter(object):
    def __init__(self, application):
        company = application.company
        self.company = {'id': company.id, 'name': company.name,
                        'company_type': company.company_type,
                        'registration_number': company.registration_number,
                        'cable_number': company.cable_number,
                        'fax_number': company.fax_number,
                        'telephone_number': company.telephone_number}
        manager = company.manager
        self.manager = {'id': manager.id, 'first_name': manager.first_name, 'last_name': manager.last_name}
        self.location = {'country': application.country, 'state': application.state}
        self.description_of_goods = application.description_of_goods
        self.status = application.status
        self.id = application.id
        consultant = application.consultant
        if application.consultant:
            self.consultant = {'id': consultant.id, 'first_name': consultant.first_name, 'last_name': consultant.last_name}


class PersonPresenter(object):
    def __init__(self, person):
        self.id = person.id
        self.first_name = person.first_name
        self.last_name = person.last_name


class InvestorPresenter(object):
    def __init__(self, investor):
        self.id = investor.id
        self.first_name = investor.person.first_name
        self.last_name = investor.person.last_name
        self.percent_owned = investor.percent_owned
        self.background_check = investor.background_check


class ShareholderPresenter(object):
    def __init__(self, investor):
        self.id = investor.id
        self.first_name = investor.person.first_name
        self.last_name = investor.person.last_name
        self.shares = investor.shares
        self.background_check = investor.background_check
