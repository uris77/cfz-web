class FormIncompleteErrorPresenter(object):
    def __init__(self, error):
        self.place_of_origin = error.place_of_origin
        self.telephone_number = error.telephone_number
